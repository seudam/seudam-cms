/**
 * section-form-contact service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::section-form-contact.section-form-contact');
