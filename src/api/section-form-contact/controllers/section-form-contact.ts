/**
 * section-form-contact controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::section-form-contact.section-form-contact');
