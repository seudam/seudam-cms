/**
 * section-form-contact router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::section-form-contact.section-form-contact');
