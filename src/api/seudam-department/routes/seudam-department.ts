/**
 * seudam-department router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::seudam-department.seudam-department');
