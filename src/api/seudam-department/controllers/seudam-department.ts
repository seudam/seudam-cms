/**
 * seudam-department controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::seudam-department.seudam-department');
