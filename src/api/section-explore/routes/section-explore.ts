/**
 * section-explore router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::section-explore.section-explore');
