/**
 * section-explore controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::section-explore.section-explore');
