/**
 * section-explore service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::section-explore.section-explore');
