/**
 * blog-hashtag controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::blog-hashtag.blog-hashtag');
