/**
 * blog-hashtag service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::blog-hashtag.blog-hashtag');
