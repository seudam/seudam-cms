/**
 * blog-hashtag router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::blog-hashtag.blog-hashtag');
