/**
 * room-featured-service controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::room-featured-service.room-featured-service');
