/**
 * room-featured-service service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::room-featured-service.room-featured-service');
