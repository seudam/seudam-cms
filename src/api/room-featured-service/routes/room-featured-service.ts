/**
 * room-featured-service router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::room-featured-service.room-featured-service');
