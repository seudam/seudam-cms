/**
 * room-service router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::room-service.room-service');
