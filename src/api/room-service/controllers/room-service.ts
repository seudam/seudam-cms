/**
 * room-service controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::room-service.room-service');
