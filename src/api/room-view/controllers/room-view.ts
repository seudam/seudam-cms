/**
 * room-view controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::room-view.room-view');
