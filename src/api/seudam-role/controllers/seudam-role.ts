/**
 * seudam-role controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::seudam-role.seudam-role');
