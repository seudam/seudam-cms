/**
 * seudam-role service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::seudam-role.seudam-role');
