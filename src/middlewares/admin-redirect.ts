module.exports = (_config, { strapi }) => {
    const redirects = ['/', '/index.html','/admin','/admin/auth/login'].map((path) => ({
        method: 'GET',
        path,
        handler: (ctx) => ctx.redirect('/admin/content-manager'),
        config: { auth: false },
    }));

    strapi.server.routes(redirects);
};