export default {
    config: {
        head: {},
        tutorials: false,
        notifications: {
            release: false
        },
        translations: {
            en: {
                "Auth.form.welcome.title": "Welcome to Seudam!",
                "Auth.form.welcome.subtitle": "Log in to your Seudam account",
                "Auth.form.email.placeholder": "e.g. seudam@gmail.com",
                "app.components.LeftMenu.navbrand.title": "Dashboard",
                "app.components.HomePage.welcomeBlock.content.again": "Let's work hard together!",
                "Settings.application.strapi-version": "CMS version",
                "Settings.application.strapiVersion": "CMS version",
            },
        },
    },
    bootstrap(app: any)
    {
        const styleElement = document.createElement("style");
        styleElement.innerText =
            `
            a[href*="cloud.strapi.io"], 
            a[href="https://strapi.io/resource-center"], 
            a[href="https://strapi.io/starters"], 
            a[href="https://strapi.io/blog/categories/tutorials"], 
            a[href*="/strapi/strapi/releases/tag/v"], 
            a[href="https://strapi.io/pricing-self-hosted"], 
            a[href="https://strapi.io/blog"],
            a[href*="/admin/plugins/cloud"] {
                display: none;
            }
            
            aside[aria-labelledby="join-the-community"] {
                display: none;
            }
            
            nav a img[alt="Application logo"] {
                width: 2.5rem;
                height: auto;
            }
        `;

        document.head.appendChild(styleElement);
    },
};
