export default ({env}) => ({
    email: {
      config: {
        provider: "nodemailer",
        providerOptions: {
          host: "smtp.gmail.com",
          port: "587",
          secure: false,
          auth: {
            user: "no-reply@seudamgroup.com",
            pass: "Adminseudam67@",
          }
          // ... any custom nodemailer options
        },
        settings: {
          defaultFrom: "no-reply@seudamgroup.com",
          defaultReplyTo: "no-reply@seudamgroup.com",
        },
      },
    },
    upload: {
          config: {
            provider: "aws-s3",
            providerOptions: {
              rootPath: env("AWS_ROOT_PATH"),
              s3Options: {
                credentials: {
                  accessKeyId: env("AWS_ACCESS_KEY_ID"),
                  secretAccessKey: env("AWS_SECRET_ACCESS_KEY"),
                },
                region: env("AWS_REGION"),
                params: {
                  ACL: env("AWS_ACL", "public-read"),
                  signedUrlExpires: env("AWS_SIGNED_URL_EXPIRES", 15 * 60),
                  Bucket: env("AWS_BUCKET_NAME"),
                },
              },
            },
            actionOptions: {
              upload: {},
              uploadStream: {},
              delete: {},
            },
          },
        },
});