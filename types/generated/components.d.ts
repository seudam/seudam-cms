import type { Schema, Attribute } from '@strapi/strapi';

export interface UsageUsageGuide extends Schema.Component {
  collectionName: 'components_usage_usage_guides';
  info: {
    displayName: 'usageGuide';
    icon: 'write';
    description: '';
  };
  attributes: {
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    bookingTitle: Attribute.String & Attribute.Required;
    bookingSteps: Attribute.Component<'shared.item-record', true> &
      Attribute.Required;
    bookingWebImage: Attribute.Media<'images'> & Attribute.Required;
    bookingMobileImage: Attribute.Media<'images'> & Attribute.Required;
    enrollTitle: Attribute.String & Attribute.Required;
    enrollSteps: Attribute.Component<'shared.sub-menu', true> &
      Attribute.Required;
    reviewTitle: Attribute.String & Attribute.Required;
    reviewSteps: Attribute.Component<'shared.sub-menu', true> &
      Attribute.Required;
  };
}

export interface SupportLostItems extends Schema.Component {
  collectionName: 'components_support_lost_items';
  info: {
    displayName: 'lostItems';
    icon: 'write';
    description: '';
  };
  attributes: {
    breadcrumbs: Attribute.Component<'shared.link', true> & Attribute.Required;
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    content: Attribute.RichText &
      Attribute.CustomField<
        'plugin::ckeditor.CKEditor',
        {
          output: 'HTML';
          preset: 'rich';
        }
      >;
  };
}

export interface SupportContactUs extends Schema.Component {
  collectionName: 'components_support_contactuses';
  info: {
    displayName: 'contactUs';
    icon: 'write';
  };
  attributes: {
    breadcrumbs: Attribute.Component<'shared.link', true> & Attribute.Required;
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    content: Attribute.Text;
    liveSupport: Attribute.Component<'seudam.social-group'> &
      Attribute.Required;
    socialContact: Attribute.Component<'seudam.social-group'> &
      Attribute.Required;
    location: Attribute.Component<'seudam.item-group'> & Attribute.Required;
    telephone: Attribute.Component<'seudam.item-group'> & Attribute.Required;
    contactForm: Attribute.Component<'global.form-contact'>;
  };
}

export interface SharedTitle extends Schema.Component {
  collectionName: 'components_shared_titles';
  info: {
    displayName: 'title';
    icon: 'house';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    titleColor: Attribute.String & Attribute.DefaultTo<'#1E1E1E'>;
    content: Attribute.Text;
  };
}

export interface SharedTimeLine extends Schema.Component {
  collectionName: 'components_shared_time_lines';
  info: {
    displayName: 'timeLine';
    icon: 'house';
  };
  attributes: {
    year: Attribute.String & Attribute.Required;
    image: Attribute.Media<'images'>;
    title: Attribute.String & Attribute.Required;
    content: Attribute.Text;
    items: Attribute.Component<'shared.title', true>;
  };
}

export interface SharedSubMenu extends Schema.Component {
  collectionName: 'components_shared_sub_menus';
  info: {
    displayName: 'subMenu';
    icon: 'house';
    description: '';
  };
  attributes: {
    route: Attribute.String & Attribute.Required;
    title: Attribute.String & Attribute.Required;
    type: Attribute.Enumeration<['page', 'link', 'file']> &
      Attribute.DefaultTo<'page'>;
    decorationImage: Attribute.Media<'images'>;
    isDisable: Attribute.Boolean & Attribute.DefaultTo<false>;
  };
}

export interface SharedSlider extends Schema.Component {
  collectionName: 'components_shared_sliders';
  info: {
    displayName: 'slider';
    icon: 'house';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    description: Attribute.Text;
    medias: Attribute.Media<'images' | 'videos', true> & Attribute.Required;
  };
}

export interface SharedSeo extends Schema.Component {
  collectionName: 'components_shared_seos';
  info: {
    displayName: 'seo';
    icon: 'search';
  };
  attributes: {
    metaTitle: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 60;
      }>;
    metaDescription: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 50;
        maxLength: 160;
      }>;
    metaImage: Attribute.Media<'images' | 'files' | 'videos'>;
    metaSocial: Attribute.Component<'shared.meta-social', true>;
    keywords: Attribute.Text;
    metaRobots: Attribute.String;
    structuredData: Attribute.JSON;
    metaViewport: Attribute.String;
    canonicalURL: Attribute.String;
  };
}

export interface SharedParagraph extends Schema.Component {
  collectionName: 'components_shared_paragraphs';
  info: {
    displayName: 'paragraph';
    icon: 'house';
    description: '';
  };
  attributes: {
    content: Attribute.Text;
  };
}

export interface SharedMetaSocial extends Schema.Component {
  collectionName: 'components_shared_meta_socials';
  info: {
    displayName: 'metaSocial';
    icon: 'project-diagram';
  };
  attributes: {
    socialNetwork: Attribute.Enumeration<['Facebook', 'Twitter']> &
      Attribute.Required;
    title: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 60;
      }>;
    description: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 65;
      }>;
    image: Attribute.Media<'images' | 'files' | 'videos'>;
  };
}

export interface SharedMenu extends Schema.Component {
  collectionName: 'components_shared_menus';
  info: {
    displayName: 'menu';
    icon: 'house';
    description: '';
  };
  attributes: {
    route: Attribute.String & Attribute.Required;
    title: Attribute.String & Attribute.Required;
    type: Attribute.Enumeration<['page', 'link', 'file']> &
      Attribute.DefaultTo<'page'>;
    isDisable: Attribute.Boolean & Attribute.DefaultTo<false>;
    icon: Attribute.String;
    decorationImage: Attribute.Media<'images'>;
    hoverHexColor: Attribute.String;
    borderHoverHexColor: Attribute.String;
    items: Attribute.Component<'shared.sub-menu', true>;
  };
}

export interface SharedMenuGroup extends Schema.Component {
  collectionName: 'components_shared_menu_groups';
  info: {
    displayName: 'menuGroup';
    icon: 'house';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    isExpanded: Attribute.Boolean & Attribute.DefaultTo<true>;
    items: Attribute.Component<'shared.menu', true>;
  };
}

export interface SharedMediaList extends Schema.Component {
  collectionName: 'components_shared_media_lists';
  info: {
    displayName: 'mediaList';
    icon: 'house';
  };
  attributes: {
    medias: Attribute.Media<'images' | 'videos', true>;
  };
}

export interface SharedLink extends Schema.Component {
  collectionName: 'components_shared_links';
  info: {
    displayName: 'link';
    icon: 'house';
    description: '';
  };
  attributes: {
    href: Attribute.String & Attribute.Required;
    label: Attribute.String & Attribute.Required;
    target: Attribute.Enumeration<['_blank', '_self']> &
      Attribute.DefaultTo<'_self'>;
    type: Attribute.Enumeration<['page', 'link', 'file']> &
      Attribute.DefaultTo<'page'>;
  };
}

export interface SharedLinkIcon extends Schema.Component {
  collectionName: 'components_shared_link_icons';
  info: {
    displayName: 'linkIcon';
    icon: 'house';
  };
  attributes: {
    href: Attribute.String & Attribute.Required;
    label: Attribute.String & Attribute.Required;
    target: Attribute.Enumeration<['_blank', '_self']> &
      Attribute.DefaultTo<'_blank'>;
    type: Attribute.Enumeration<['page', 'link', 'file']> &
      Attribute.DefaultTo<'link'>;
    iconImage: Attribute.Media<'images'>;
  };
}

export interface SharedLinkGroup extends Schema.Component {
  collectionName: 'components_shared_link_groups';
  info: {
    displayName: 'linkGroup';
    icon: 'house';
  };
  attributes: {
    title: Attribute.String;
    content: Attribute.Text;
    items: Attribute.Component<'shared.link', true>;
  };
}

export interface SharedLinkFile extends Schema.Component {
  collectionName: 'components_shared_link_files';
  info: {
    displayName: 'linkFile';
    icon: 'house';
  };
  attributes: {
    label: Attribute.String & Attribute.Required;
    target: Attribute.Enumeration<['_self', '_blank']> &
      Attribute.DefaultTo<'_self'>;
    href: Attribute.Media<'files'> & Attribute.Required;
  };
}

export interface SharedLinkButton extends Schema.Component {
  collectionName: 'components_shared_link_buttons';
  info: {
    displayName: 'linkButton';
    icon: 'house';
    description: '';
  };
  attributes: {
    class: Attribute.String;
    label: Attribute.String & Attribute.Required;
    href: Attribute.String & Attribute.Required;
    target: Attribute.Enumeration<['_blank', '_self']> &
      Attribute.DefaultTo<'_self'>;
    type: Attribute.Enumeration<['page', 'link', 'file', 'email', 'phone']> &
      Attribute.DefaultTo<'page'>;
  };
}

export interface SharedLabel extends Schema.Component {
  collectionName: 'components_shared_labels';
  info: {
    displayName: 'label';
    icon: 'house';
  };
  attributes: {
    content: Attribute.String & Attribute.Required;
  };
}

export interface SharedItem extends Schema.Component {
  collectionName: 'components_shared_items';
  info: {
    displayName: 'item';
    icon: 'house';
    description: '';
  };
  attributes: {
    label: Attribute.String;
    content: Attribute.String & Attribute.Required;
    iconImage: Attribute.Media<'images'>;
    iconSelectedImage: Attribute.Media<'images'>;
    iconFont: Attribute.String;
    isDisable: Attribute.Boolean & Attribute.DefaultTo<false>;
  };
}

export interface SharedItemValue extends Schema.Component {
  collectionName: 'components_shared_item_values';
  info: {
    displayName: 'itemValue';
    icon: 'house';
  };
  attributes: {
    text: Attribute.String & Attribute.Required;
    value: Attribute.String & Attribute.Required;
  };
}

export interface SharedItemReport extends Schema.Component {
  collectionName: 'components_shared_item_reports';
  info: {
    displayName: 'itemReport';
    icon: 'house';
    description: '';
  };
  attributes: {
    label: Attribute.String;
    title: Attribute.String & Attribute.Required;
    value: Attribute.String & Attribute.Required;
    unit: Attribute.String;
  };
}

export interface SharedItemRecord extends Schema.Component {
  collectionName: 'components_shared_item_records';
  info: {
    displayName: 'itemRecord';
    icon: 'house';
    description: '';
  };
  attributes: {
    label: Attribute.String & Attribute.Required;
    value: Attribute.String & Attribute.Required;
    title: Attribute.String;
    description: Attribute.String;
  };
}

export interface SharedItemFeature extends Schema.Component {
  collectionName: 'components_shared_item_features';
  info: {
    displayName: 'itemFeature';
    icon: 'house';
    description: '';
  };
  attributes: {
    image: Attribute.Media<'images'>;
    title: Attribute.String & Attribute.Required;
    content: Attribute.RichText &
      Attribute.CustomField<
        'plugin::ckeditor.CKEditor',
        {
          output: 'HTML';
          preset: 'rich';
        }
      >;
  };
}

export interface SharedHtml extends Schema.Component {
  collectionName: 'components_shared_htmls';
  info: {
    displayName: 'html';
    icon: 'house';
  };
  attributes: {
    title: Attribute.String;
    content: Attribute.RichText &
      Attribute.CustomField<
        'plugin::ckeditor.CKEditor',
        {
          output: 'HTML';
          preset: 'rich';
        }
      >;
  };
}

export interface SharedHeader extends Schema.Component {
  collectionName: 'components_shared_headers';
  info: {
    displayName: 'header';
    icon: 'house';
    description: '';
  };
  attributes: {
    title: Attribute.String;
  };
}

export interface SharedContact extends Schema.Component {
  collectionName: 'components_shared_contacts';
  info: {
    displayName: 'contact';
    icon: 'house';
    description: '';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    address: Attribute.String;
    phoneDisplay: Attribute.String;
    phoneNumber: Attribute.String;
    email: Attribute.String;
    emailDisplay: Attribute.String;
    url: Attribute.Text;
    image: Attribute.Media<'images'>;
  };
}

export interface SeudamGoService extends Schema.Component {
  collectionName: 'components_seudam_go_services';
  info: {
    displayName: 'service';
    icon: 'write';
    description: '';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    serviceImage: Attribute.Media<'images'> & Attribute.Required;
    titleImage: Attribute.Media<'images'>;
    priceImage: Attribute.Media<'images'> & Attribute.Required;
    height: Attribute.Integer;
  };
}

export interface SeudamGoSectionWhySeudamGo extends Schema.Component {
  collectionName: 'components_seudam_go_section_why_seudam_gos';
  info: {
    displayName: 'sectionWhySeudamGo';
    icon: 'write';
    description: '';
  };
  attributes: {
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    description: Attribute.Text & Attribute.Required;
    features: Attribute.Component<'shared.item', true> & Attribute.Required;
    tabs: Attribute.Component<'shared.item', true> & Attribute.Required;
    sliders: Attribute.Component<'shared.slider', true> & Attribute.Required;
    button: Attribute.Component<'shared.link-button'> & Attribute.Required;
  };
}

export interface SeudamGoSectionTravel extends Schema.Component {
  collectionName: 'components_seudam_go_section_travels';
  info: {
    displayName: 'sectionTravel';
    icon: 'write';
  };
  attributes: {
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    features: Attribute.Component<'shared.item-report', true> &
      Attribute.Required;
  };
}

export interface SeudamGoSectionServices extends Schema.Component {
  collectionName: 'components_seudam_go_section_services';
  info: {
    displayName: 'sectionServices';
    icon: 'write';
    description: '';
  };
  attributes: {
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    services: Attribute.Component<'shared.sub-menu', true> & Attribute.Required;
  };
}

export interface SeudamGoSectionSchedules extends Schema.Component {
  collectionName: 'components_seudam_go_section_schedules';
  info: {
    displayName: 'sectionSchedules';
    icon: 'write';
    description: '';
  };
  attributes: {
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    schedules: Attribute.Component<'seudam-go.schedule', true> &
      Attribute.Required;
  };
}

export interface SeudamGoSectionPopularRoutes extends Schema.Component {
  collectionName: 'components_seudam_go_section_popular_routes';
  info: {
    displayName: 'sectionPopularRoutes';
    icon: 'write';
  };
  attributes: {
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    groups: Attribute.Component<'seudam-go.group-popular-routes', true> &
      Attribute.Required;
  };
}

export interface SeudamGoSectionPiers extends Schema.Component {
  collectionName: 'components_seudam_go_section_piers';
  info: {
    displayName: 'sectionPiers';
    icon: 'write';
  };
  attributes: {
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    contacts: Attribute.Component<'shared.contact', true> & Attribute.Required;
  };
}

export interface SeudamGoSchedule extends Schema.Component {
  collectionName: 'components_seudam_go_schedules';
  info: {
    displayName: 'schedule';
    icon: 'write';
  };
  attributes: {
    label: Attribute.String;
    title: Attribute.String & Attribute.Required;
    highSeasonImage: Attribute.Media<'images'> & Attribute.Required;
    lowSeasonImage: Attribute.Media<'images'> & Attribute.Required;
  };
}

export interface SeudamGoRentalServices extends Schema.Component {
  collectionName: 'components_seudam_go_rental_services';
  info: {
    displayName: 'rentalServices';
    icon: 'write';
    description: '';
  };
  attributes: {
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    content: Attribute.Text;
    services: Attribute.Component<'seudam-go.service', true> &
      Attribute.Required;
  };
}

export interface SeudamGoPopularRoute extends Schema.Component {
  collectionName: 'components_seudam_go_popular_routes';
  info: {
    displayName: 'popularRoute';
    icon: 'write';
  };
  attributes: {
    from: Attribute.String & Attribute.Required;
    to: Attribute.String & Attribute.Required;
    daily: Attribute.String & Attribute.Required;
    time: Attribute.String & Attribute.Required;
    button: Attribute.Component<'shared.link-button'> & Attribute.Required;
  };
}

export interface SeudamGoGroupPopularRoutes extends Schema.Component {
  collectionName: 'components_seudam_go_group_popular_routes';
  info: {
    displayName: 'groupPopularRoutes';
    icon: 'write';
  };
  attributes: {
    label: Attribute.String & Attribute.Required;
    title: Attribute.Component<'shared.label', true> & Attribute.Required;
    routes: Attribute.Component<'seudam-go.popular-route', true> &
      Attribute.Required;
  };
}

export interface SeudamBiteAccommodation extends Schema.Component {
  collectionName: 'components_seudam_bite_accommodations';
  info: {
    displayName: 'accommodation';
    icon: 'write';
  };
  attributes: {
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    content: Attribute.Text;
    subTitle: Attribute.String & Attribute.Required;
    videos: Attribute.Media<'videos', true>;
  };
}

export interface SeudamSocialGroup extends Schema.Component {
  collectionName: 'components_seudam_social_groups';
  info: {
    displayName: 'socialGroup';
    icon: 'house';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    content: Attribute.String;
    socials: Attribute.Component<'shared.link-icon', true> & Attribute.Required;
  };
}

export interface SeudamItemGroup extends Schema.Component {
  collectionName: 'components_seudam_item_groups';
  info: {
    displayName: 'itemGroup';
    icon: 'house';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    content: Attribute.String;
    items: Attribute.Component<'shared.item-record', true> & Attribute.Required;
  };
}

export interface SeudamFeatureItem extends Schema.Component {
  collectionName: 'components_seudam_feature_items';
  info: {
    displayName: 'featureItem';
    icon: 'write';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    content: Attribute.Text;
    image: Attribute.Media<'images'> & Attribute.Required;
    attributes: Attribute.Component<'shared.label', true> & Attribute.Required;
    button: Attribute.Component<'shared.link-button'> & Attribute.Required;
  };
}

export interface SeudamFeatureGroup extends Schema.Component {
  collectionName: 'components_seudam_feature_groups';
  info: {
    displayName: 'featureGroup';
    icon: 'write';
  };
  attributes: {
    label: Attribute.String & Attribute.Required;
    title: Attribute.String & Attribute.Required;
    content: Attribute.String;
    features: Attribute.Component<'seudam.feature-item', true> &
      Attribute.Required;
  };
}

export interface PolicyTerm extends Schema.Component {
  collectionName: 'components_policy_terms';
  info: {
    displayName: 'term';
    icon: 'write';
  };
  attributes: {
    breadcrumbs: Attribute.Component<'shared.link', true> & Attribute.Required;
    image: Attribute.Media<'images'>;
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    content: Attribute.RichText &
      Attribute.CustomField<
        'plugin::ckeditor.CKEditor',
        {
          output: 'HTML';
          preset: 'rich';
        }
      >;
  };
}

export interface PolicyPrivatePolicy extends Schema.Component {
  collectionName: 'components_policy_private_policies';
  info: {
    displayName: 'privatePolicy';
  };
  attributes: {
    breadcrumbs: Attribute.Component<'shared.link', true> & Attribute.Required;
    image: Attribute.Media<'images'>;
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    content: Attribute.RichText &
      Attribute.CustomField<
        'plugin::ckeditor.CKEditor',
        {
          output: 'HTML';
          preset: 'rich';
        }
      >;
  };
}

export interface PolicyCondition extends Schema.Component {
  collectionName: 'components_policy_conditions';
  info: {
    displayName: 'condition';
    icon: 'write';
  };
  attributes: {
    breadcrumbs: Attribute.Component<'shared.link', true> & Attribute.Required;
    image: Attribute.Media<'images'>;
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    content: Attribute.RichText &
      Attribute.CustomField<
        'plugin::ckeditor.CKEditor',
        {
          output: 'HTML';
          preset: 'rich';
        }
      >;
  };
}

export interface NewsPromotion extends Schema.Component {
  collectionName: 'components_news_promotions';
  info: {
    displayName: 'promotion';
    icon: 'write';
  };
  attributes: {
    breadcrumbs: Attribute.Component<'shared.link', true> & Attribute.Required;
    header: Attribute.Component<'shared.header'> & Attribute.Required;
  };
}

export interface NewsGallery extends Schema.Component {
  collectionName: 'components_news_galleries';
  info: {
    displayName: 'gallery';
    icon: 'write';
  };
  attributes: {
    breadcrumbs: Attribute.Component<'shared.link', true> & Attribute.Required;
    categories: Attribute.Component<'gallery.gallery-category', true> &
      Attribute.Required;
  };
}

export interface IntroducePartner extends Schema.Component {
  collectionName: 'components_introduce_partners';
  info: {
    displayName: 'partner';
    icon: 'write';
  };
  attributes: {
    tabs: Attribute.Component<'seudam.feature-group', true> &
      Attribute.Required;
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    steps: Attribute.Component<'shared.item', true> & Attribute.Required;
    subTitle: Attribute.String & Attribute.Required;
    subContent: Attribute.Text;
    form: Attribute.Component<'global.form-contact'>;
  };
}

export interface IntroduceCareer extends Schema.Component {
  collectionName: 'components_introduce_careers';
  info: {
    displayName: 'career';
    icon: 'write';
  };
  attributes: {
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    content: Attribute.Text;
    jobs: Attribute.Component<'career.job', true>;
    subTitle: Attribute.String & Attribute.Required;
    subContent: Attribute.Text;
    activities: Attribute.Component<'shared.sub-menu', true> &
      Attribute.Required;
    button: Attribute.Component<'shared.link-button'> & Attribute.Required;
  };
}

export interface IntroduceAboutUs extends Schema.Component {
  collectionName: 'components_introduce_aboutuses';
  info: {
    displayName: 'aboutUs';
    icon: 'write';
  };
  attributes: {
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    content: Attribute.Text;
    histories: Attribute.Component<'shared.time-line', true> &
      Attribute.Required;
    mission: Attribute.Component<'about-us.mission'> & Attribute.Required;
    culture: Attribute.Component<'about-us.core-values'> & Attribute.Required;
    core: Attribute.Component<'about-us.core-values'> & Attribute.Required;
    people: Attribute.Component<'about-us.key-people'> & Attribute.Required;
  };
}

export interface HomeProduct extends Schema.Component {
  collectionName: 'components_home_products';
  info: {
    displayName: 'product';
    icon: 'write';
    description: '';
  };
  attributes: {
    header: Attribute.Component<'shared.header'>;
    name: Attribute.String;
    backgroundImage: Attribute.Media<'images'> & Attribute.Required;
    filterImage: Attribute.Media<'images'> & Attribute.Required;
    titleImage: Attribute.Media<'images'> & Attribute.Required;
    sloganImage: Attribute.Media<'images'> & Attribute.Required;
    content: Attribute.Text & Attribute.Required;
    features: Attribute.Component<'shared.item', true>;
    button: Attribute.Component<'shared.link-button'> & Attribute.Required;
  };
}

export interface HomeOurPartnerships extends Schema.Component {
  collectionName: 'components_home_our_partnerships';
  info: {
    displayName: 'ourPartnerships';
    icon: 'write';
  };
  attributes: {
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    images: Attribute.Media<'images', true>;
  };
}

export interface HomeOurGallery extends Schema.Component {
  collectionName: 'components_home_our_galleries';
  info: {
    displayName: 'ourGallery';
    icon: 'write';
  };
  attributes: {
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    images: Attribute.Media<'images', true>;
    button: Attribute.Component<'shared.link-button'> & Attribute.Required;
  };
}

export interface HomeIntroduce extends Schema.Component {
  collectionName: 'components_home_introduces';
  info: {
    displayName: 'introduce';
    icon: 'write';
    description: '';
  };
  attributes: {
    backgroundImage: Attribute.Media<'images'> & Attribute.Required;
    logoImage: Attribute.Media<'images'> & Attribute.Required;
    sloganImage: Attribute.Media<'images'> & Attribute.Required;
    leftImage: Attribute.Media<'images'> & Attribute.Required;
    topRightImage: Attribute.Media<'images'> & Attribute.Required;
    bottomRightImage: Attribute.Media<'images'> & Attribute.Required;
    firstParagraph: Attribute.Text & Attribute.Required;
    secondParagraph: Attribute.Text;
    button: Attribute.Component<'shared.link-button'> & Attribute.Required;
  };
}

export interface HomeGroupProducts extends Schema.Component {
  collectionName: 'components_home_group_products';
  info: {
    displayName: 'groupProducts';
    icon: 'write';
  };
  attributes: {
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    products: Attribute.Component<'home.product', true>;
  };
}

export interface HomeGroupExplore extends Schema.Component {
  collectionName: 'components_home_group_explores';
  info: {
    displayName: 'groupExplore';
    icon: 'write';
  };
  attributes: {
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    groups: Attribute.Component<'home.explore', true>;
  };
}

export interface HomeGroupComments extends Schema.Component {
  collectionName: 'components_home_group_comments';
  info: {
    displayName: 'groupComments';
    icon: 'write';
    description: '';
  };
  attributes: {
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    comments: Attribute.Component<'home.comment', true>;
    button: Attribute.Component<'shared.link-button'> & Attribute.Required;
    facebookImage: Attribute.Media<'images'> & Attribute.Required;
    googleImage: Attribute.Media<'images'> & Attribute.Required;
    tiktokImage: Attribute.Media<'images'> & Attribute.Required;
    facebookUrl: Attribute.String & Attribute.Required;
    googleUrl: Attribute.String & Attribute.Required;
    tiktokUrl: Attribute.String & Attribute.Required;
  };
}

export interface HomeExplore extends Schema.Component {
  collectionName: 'components_home_explores';
  info: {
    displayName: 'explore';
    icon: 'write';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    links: Attribute.Component<'shared.link', true>;
  };
}

export interface HomeComment extends Schema.Component {
  collectionName: 'components_home_comments';
  info: {
    displayName: 'comment';
    icon: 'write';
  };
  attributes: {
    social: Attribute.Enumeration<['tiktok', 'google', 'facebook']> &
      Attribute.Required;
    commentImage: Attribute.Media<'images'> & Attribute.Required;
  };
}

export interface HomeAcceptPayment extends Schema.Component {
  collectionName: 'components_home_accept_payments';
  info: {
    displayName: 'acceptPayment';
    icon: 'write';
    description: '';
  };
  attributes: {
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    images: Attribute.Media<'images', true>;
  };
}

export interface HelpUsageProcedure extends Schema.Component {
  collectionName: 'components_help_usage_procedures';
  info: {
    displayName: 'usageProcedure';
    icon: 'write';
  };
  attributes: {
    breadcrumbs: Attribute.Component<'shared.link', true> & Attribute.Required;
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    onlineServices: Attribute.Component<'seudam.social-group'> &
      Attribute.Required;
    offlineServices: Attribute.Component<'seudam.item-group'> &
      Attribute.Required;
    body: Attribute.Component<'usage.usage-guide'> & Attribute.Required;
  };
}

export interface HelpFeedback extends Schema.Component {
  collectionName: 'components_help_feedbacks';
  info: {
    displayName: 'feedback';
    icon: 'write';
    description: '';
  };
  attributes: {
    breadcrumbs: Attribute.Component<'shared.link', true> & Attribute.Required;
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    visionSection: Attribute.Component<'shared.title'> & Attribute.Required;
    satisfiedSection: Attribute.Component<'shared.title'> & Attribute.Required;
    rateSection: Attribute.Component<'shared.title'> & Attribute.Required;
    recommendSection: Attribute.Component<'shared.title'> & Attribute.Required;
    commitment: Attribute.Text;
  };
}

export interface HelpFaqs extends Schema.Component {
  collectionName: 'components_help_faqs';
  info: {
    displayName: 'faqs';
    icon: 'write';
    description: '';
  };
  attributes: {
    breadcrumbs: Attribute.Component<'shared.link', true> & Attribute.Required;
    header: Attribute.Component<'shared.header'> & Attribute.Required;
    categories: Attribute.Component<'faqs.faq-category', true> &
      Attribute.Required;
  };
}

export interface GlobalFormContact extends Schema.Component {
  collectionName: 'components_global_form_contacts';
  info: {
    displayName: 'formContact';
    icon: 'write';
    description: '';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    content: Attribute.String & Attribute.Required;
    backgroundImage: Attribute.Media<'images'> & Attribute.Required;
    button: Attribute.Component<'shared.link-button'>;
  };
}

export interface GlobalFormBooking extends Schema.Component {
  collectionName: 'components_global_form_bookings';
  info: {
    displayName: 'formBooking';
    icon: 'write';
    description: '';
  };
  attributes: {
    tabs: Attribute.Component<'shared.item', true> & Attribute.Required;
    termTitle: Attribute.String & Attribute.Required;
    termMessage: Attribute.String & Attribute.Required;
    termContent: Attribute.Text & Attribute.Required;
    passengerTitle: Attribute.String & Attribute.Required;
    passengerContent: Attribute.Component<'shared.paragraph', true> &
      Attribute.Required;
    petTitle: Attribute.String & Attribute.Required;
    petContent: Attribute.Component<'shared.paragraph', true> &
      Attribute.Required;
    logoImage: Attribute.Media<'images'> & Attribute.Required;
    logoTextImage: Attribute.Media<'images'> & Attribute.Required;
  };
}

export interface GlobalBanner extends Schema.Component {
  collectionName: 'components_global_banners';
  info: {
    displayName: 'banner';
    icon: 'write';
  };
  attributes: {
    medias: Attribute.Media<'images' | 'videos', true> & Attribute.Required;
  };
}

export interface GalleryGalleryCategory extends Schema.Component {
  collectionName: 'components_gallery_gallery_categories';
  info: {
    displayName: 'galleryCategory';
    icon: 'write';
    description: '';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    content: Attribute.Text;
    pages: Attribute.Component<'shared.media-list', true>;
  };
}

export interface FaqsFaq extends Schema.Component {
  collectionName: 'components_faqs_faqs';
  info: {
    displayName: 'faq';
    icon: 'write';
    description: '';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    items: Attribute.Component<'shared.html', true>;
  };
}

export interface FaqsFaqCategory extends Schema.Component {
  collectionName: 'components_faqs_faq_categories';
  info: {
    displayName: 'faqCategory';
    icon: 'write';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    faqs: Attribute.Component<'faqs.faq', true>;
  };
}

export interface CareerJob extends Schema.Component {
  collectionName: 'components_career_jobs';
  info: {
    displayName: 'job';
    icon: 'write';
    description: '';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    location: Attribute.String & Attribute.Required;
    type: Attribute.String & Attribute.Required;
    benefits: Attribute.Component<'shared.label', true> & Attribute.Required;
    button: Attribute.Component<'shared.link-file'> & Attribute.Required;
    isDisable: Attribute.Boolean & Attribute.DefaultTo<false>;
  };
}

export interface AboutUsMission extends Schema.Component {
  collectionName: 'components_about_us_missions';
  info: {
    displayName: 'mission';
    icon: 'write';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    items: Attribute.Component<'shared.html', true> & Attribute.Required;
  };
}

export interface AboutUsKeyPeople extends Schema.Component {
  collectionName: 'components_about_us_key_people';
  info: {
    displayName: 'keyPeople';
    icon: 'write';
    description: '';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    content: Attribute.Text;
    persons: Attribute.Component<'shared.item-feature', true> &
      Attribute.Required;
  };
}

export interface AboutUsCoreValues extends Schema.Component {
  collectionName: 'components_about_us_core_values';
  info: {
    displayName: 'coreValues';
    icon: 'write';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    values: Attribute.Component<'shared.item-feature', true> &
      Attribute.Required;
  };
}

declare module '@strapi/types' {
  export module Shared {
    export interface Components {
      'usage.usage-guide': UsageUsageGuide;
      'support.lost-items': SupportLostItems;
      'support.contact-us': SupportContactUs;
      'shared.title': SharedTitle;
      'shared.time-line': SharedTimeLine;
      'shared.sub-menu': SharedSubMenu;
      'shared.slider': SharedSlider;
      'shared.seo': SharedSeo;
      'shared.paragraph': SharedParagraph;
      'shared.meta-social': SharedMetaSocial;
      'shared.menu': SharedMenu;
      'shared.menu-group': SharedMenuGroup;
      'shared.media-list': SharedMediaList;
      'shared.link': SharedLink;
      'shared.link-icon': SharedLinkIcon;
      'shared.link-group': SharedLinkGroup;
      'shared.link-file': SharedLinkFile;
      'shared.link-button': SharedLinkButton;
      'shared.label': SharedLabel;
      'shared.item': SharedItem;
      'shared.item-value': SharedItemValue;
      'shared.item-report': SharedItemReport;
      'shared.item-record': SharedItemRecord;
      'shared.item-feature': SharedItemFeature;
      'shared.html': SharedHtml;
      'shared.header': SharedHeader;
      'shared.contact': SharedContact;
      'seudam-go.service': SeudamGoService;
      'seudam-go.section-why-seudam-go': SeudamGoSectionWhySeudamGo;
      'seudam-go.section-travel': SeudamGoSectionTravel;
      'seudam-go.section-services': SeudamGoSectionServices;
      'seudam-go.section-schedules': SeudamGoSectionSchedules;
      'seudam-go.section-popular-routes': SeudamGoSectionPopularRoutes;
      'seudam-go.section-piers': SeudamGoSectionPiers;
      'seudam-go.schedule': SeudamGoSchedule;
      'seudam-go.rental-services': SeudamGoRentalServices;
      'seudam-go.popular-route': SeudamGoPopularRoute;
      'seudam-go.group-popular-routes': SeudamGoGroupPopularRoutes;
      'seudam-bite.accommodation': SeudamBiteAccommodation;
      'seudam.social-group': SeudamSocialGroup;
      'seudam.item-group': SeudamItemGroup;
      'seudam.feature-item': SeudamFeatureItem;
      'seudam.feature-group': SeudamFeatureGroup;
      'policy.term': PolicyTerm;
      'policy.private-policy': PolicyPrivatePolicy;
      'policy.condition': PolicyCondition;
      'news.promotion': NewsPromotion;
      'news.gallery': NewsGallery;
      'introduce.partner': IntroducePartner;
      'introduce.career': IntroduceCareer;
      'introduce.about-us': IntroduceAboutUs;
      'home.product': HomeProduct;
      'home.our-partnerships': HomeOurPartnerships;
      'home.our-gallery': HomeOurGallery;
      'home.introduce': HomeIntroduce;
      'home.group-products': HomeGroupProducts;
      'home.group-explore': HomeGroupExplore;
      'home.group-comments': HomeGroupComments;
      'home.explore': HomeExplore;
      'home.comment': HomeComment;
      'home.accept-payment': HomeAcceptPayment;
      'help.usage-procedure': HelpUsageProcedure;
      'help.feedback': HelpFeedback;
      'help.faqs': HelpFaqs;
      'global.form-contact': GlobalFormContact;
      'global.form-booking': GlobalFormBooking;
      'global.banner': GlobalBanner;
      'gallery.gallery-category': GalleryGalleryCategory;
      'faqs.faq': FaqsFaq;
      'faqs.faq-category': FaqsFaqCategory;
      'career.job': CareerJob;
      'about-us.mission': AboutUsMission;
      'about-us.key-people': AboutUsKeyPeople;
      'about-us.core-values': AboutUsCoreValues;
    }
  }
}
